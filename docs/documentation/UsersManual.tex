\documentclass[topmargin=-20pt,pdftex,a4paper,11pt]{report}
\usepackage[utf8x]{inputenc}
% Was put for using nested textbf and textsc
\usepackage[T1]{fontenc}
% uses the full page as much as possible
%\usepackage{fullpage}
\usepackage{a4wide}
% This should be removed because colour doesn't look good on print
% \usepackage{color}
% This is for putting headers on every page
\usepackage{pagestyle}
% For creating hyperlinks within the documents.
\usepackage[colorlinks=true,urlcolor=black,linkcolor=black,citecolor=black,filecolor=black]{hyperref}
\usepackage{textcomp}
\usepackage{slantsc}
% uses the graphicx package for images
% \usepackage[pdftex]{graphicx}
\usepackage{graphicx}
% This package does something which I need to figure out. Introduced by
% Sebastian
\usepackage{subfigure}
% Following three packages included for including Tex graph example
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
% Package to display tables spread across pages, preserving headers and footers
% of the table
\usepackage{longtable}
% for links in bibliography
\usepackage{url}
% For floating images
% \usepackage{float}


%% Some handy new commands for this document
\newcommand{\javacc}{\textsf{JavaCC}}
\newcommand{\dragon}{\textbf{\textsf{DragonFish}}}
\newcommand{\prefuse}{\textbf{prefuse}}

\newcommand{\attn}[1]{\textbf{\emph{#1}}}
\newcommand{\class}[1]{\texttt{#1}}
\newcommand{\package}[1]{\texttt{rmit.ai.bcp.fsm#1}}
\newcommand{\method}[1]{\textit{#1(\ldots)}}
% Creates a thick horizontal rule
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% Header for all the pages
\allhead{\dragon{} v1.0 - An IDE to develop Finite State Machines for
BCP}{}{\thepage}

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

% main document starts from here
\begin{document}
  % This would be on the first page of the Report style document
  %\maketitle
  \input{./UManual_title.tex} \label{doc_top}

  % Prevent this page from having any headers
  \thispagestyle{empty} % Redundant at this location
  \newpage
  \mbox{}
  \newpage

  % This is the second page of the Report style document
  \begin{abstract}
  \dragon{} is a platform and tool to build and edit Finite State Automata or
  Transition Systems interactively, specifically designed to support research in the area
  of Behaviour Composition. \dragon{} is able to build the state machines
  incrementally, adding and deleting states and transitions. This
  document is a guide to users who wish to use this tool and create their own
  machines starting from a base machine, defined in DOT format.
  \end{abstract}
  
  \newpage
   % Prevent this page from having any headers
  \thispagestyle{empty} % Redundant at this location
  \mbox{}
  \newpage

  \tableofcontents
   \thispagestyle{empty}
  \listoffigures
   \thispagestyle{empty}
  \newpage

  % This command resets the numbering and removes the chapter numbering
  % In absence of a Chapter or this command, the sections have numbers like 0.1,
  % 0.2, 0.1.1 etc.
  \renewcommand*\thesection{\arabic{section}}
  
  % ============================================================================
  % ============================================================================
  
  % Actual content of the report begins from here which would begin from the
  % third page in the Report style document
  \section{Introduction}
  \dragon{} is an IDE tool to build finite-state automata or transition
  systems specifically designed to support research in the area of Behaviour
  Composition. Behaviour Composition Problem (BCP) is the problem of
  synthesising a fully controllable target behaviour from a set of available
  partially controllable behaviours that are to execute within a shared
  partially predictable, but fully observable, environment. Behaviours are
  represented with a sort of non-deterministic transition systems, whose
  transitions are conditioned on the current state of the environment, also
  represented as a non-deterministic finite transition system. On the other
  hand, the target behaviour is assumed to be fully deterministic and stands for
  the behaviour that the system as a whole needs to guarantee
  \cite{DeGiacomoS:IJCAI07}. Although a rigorous mathematical treatment of
  Finite State Automata and/or transition systems is beyond the scope of this
  document, concisely a Finite State Machine (FSM) is a 5-tuple consisting of
  set of finite states over a set of finite accepting and response alphabet and
  a transition function determining the state transitions
  \cite{Minsky:CompMachines}. \dragon{} will allow users to draw FSMs and/or
  transition systems and then export them into the standard .dot format for
  graph, as well as to load .dot files (see Figure~\ref{fig:sample_fsm}),
  visualize them, and allow editing.
  
  % ============================================================================
  % ============================================================================  
  \section{The DOT language and Behaviour Composition} \label{desc_dotlang}
  DOT Language \cite{web:dotlang} is a comprehensive and powerful language
  to represent graphs of any sort \cite{Wiki:GraphTypes} (except Mixed Graphs)
  in a concise manner. DOT (.dot) files are simple text files written in a
  format specified by DOT language. It is important to note that in the current
  build of the system, not all the features of the language have been leveraged
  and only a subset of features/syntax is available.
  
  The current build of \dragon{} uses only the following information/format of
  the dot-formatted file. In terms of grammars, terminals are shown in
  \textbf{bold} font and non-terminals in \textit{italics}. Literal characters
  are given in single quotes. Parentheses '(' and ')' indicate grouping when
  needed. Square brackets '[' and ']' enclose optional items. Vertical bars '|'
  separate alternatives. See Figure~\ref{fig:sample_fsm} for a sample .dot
  file.
  % Inserting the sample FSM with its label and sample DOT notation
  \input{shared/sample_fsm.tex}

  \begin{center}
  \begin{tabular}[t]{rcp{8cm}}
  % Table Data
  \textit{graph}      & : & (\textbf{graph} | \textbf{digraph}) [ \textit{ID} ] \textbf{'\{'} \textit{stmt\_list} \textbf{'\}}' \\
  \textit{stmt\_list} & : & [ \textit{stmt} [ \textbf{';'} ] [ \textit{stmt\_list} ] ] \\
  \textit{stmt}       & : & \textit{node\_stmt} | \textit{edge\_stmt} \\
  \textit{edge\_stmt} & : & \textit{node\_id} \textit{edgeRHS} [ \textit{attr\_list} ] \\
  \textit{edgeRHS}    & : & \textit{edgeop} \textit{node\_id} \\
  \textit{node\_stmt} & : & \textit{node\_id} [ \textit{attr\_list} ] \\
  \textit{node\_id}   & : & \textit{ID} \\
  \textit{attr\_list} & : & \textbf{'['} [ \textit{a\_list} ] \textbf{']'} [ \textit{attr\_list} ] \\
  \textit{a\_list}    & : & \textit{ID} [ \textbf{'='} \textit{ID} ] [ \textbf{','} ] [ \textit{a\_list} ] \\
  \end{tabular}
  \end{center}
%   
  In a BCP, behaviours are represented as transition systems and so are the
  environments in which these behaviours are conditioned
  \cite{DeGiacomoS:IJCAI07}. These transition systems and/or Finite State
  Machines are represented in the form of graphs defined in the .dot files,
  where the states are the nodes in a graph and transitions are the edges (see
  Figure~\ref{fig:sample_fsm}). Due to its simplicity and compactness and its
  ability represent many types of graphs in a concise form, DOT was chosen as
  the format to represent the transition systems pertinent to BCP and this tool.
  There is nothing more special about these files except for the following two
  things which are required in any transition system:  
  \begin{itemize}
    \item The states (nodes) in a FSM can either be normal, initial or final;
    where multiple states can be initial/final. This corresponds to the fact
    that a machine starts working from a given state and then finishes its
    operation when it reaches any given final state. To specify this information
    in a dot file within the constraints of dot format without extending it,
    there is a special label within the label itself,
    \texttt{initial=true|false} and \texttt{final=true|false}.

    \item The transitions (edges) can also be marked whether they are legal or
    not, pertinent to BCP, and if legal then for which states would they be
    legal. This information is stored in the label for transitions,
    \texttt{legal=*|N$_1$,N$_2$,N$_3$,...,N$_n$}, where N$_n$ are the states.
  \end{itemize}
  
  % ============================================================================
  % ============================================================================
  \section{Core Technologies}   
   These tools/libraries form the core technology behind various parts of the
   system.
   \begin{description}
   \item[GraphViz's DOT Language] The DOT Language. Refer to
   sec~\ref{desc_dotlang} for a detailed description of the language and reason
   behind this choice for graph representation.
   
   \item[\prefuse{} Visualisation Toolkit] \prefuse{} is a set of software tools
   for creating rich interactive data visualisations \cite{web:prefuse}. The
   original \prefuse{} toolkit provides a visualisation framework for the Java
   programming language. It supports a rich set of features for data
   modeling, visualisation, and interaction. It provides optimised data
   structures for tables, graphs, and trees, a host of layout and visual
   encoding techniques, and support for animation, dynamic queries, integrated
   search  and database connectivity. It is written in Java, using the
   Java 2D graphics library, and can be easily integrated into Java Swing
   applications or web applets.
   \end{description}
  
  % ============================================================================
  % ============================================================================
  \section{Features}
  \label{features}
  %The main features of the project/functionality/achievement of the final
  %version.
  \dragon{} has many features, which have been designed and implemented
  specifically for the purpose of supporting research in Behaviour Composition.
  New features/functionality may be proposed in future and existing ones may be
  improved.
  
  \begin{description}
  % The accepted subset of features for the current version
  \item[Current Features] (Release v1.0; as of \today.)
    \begin{enumerate}
      \item Read and parse the machines from files in DOT Language (.dot
      extension). See Fig~\ref{shot2} and Fig~\ref{shot8}.
      
      \item Display/Visualise one loaded machine at a time on the screen. Labels
      are displayed for states only (see Fig~\ref{shot3}), although being stored
      for both states and transitions within the machine itself. This
      is a limitation in the current system and will be provided in the future
      builds of the system.
      
      \item Name of the machine displayed in the title bar.
      
      \item Edit the loaded machines (rename, add and delete states and
      transitions). See Fig~\ref{shot6}.
      
      \item Save the edited machine onto the disk in a file using DOT syntax
      (this is the same file from which the machine was loaded, see
      Fig~\ref{shot3}). Saving machines to different files is currently not
      available.
      
      \item Select multiple states using the \texttt{Ctrl+click} modifier key
      (see Fig~\ref{shot4}).
      
      \item Highlight states by hovering on them with the mouse pointer, which
      also highlights connected states (see Fig~\ref{shot4}).
      
      \item Use a selection rectangle / rubber-band select to select multiple
      states simultaneously using click-and-drag (see Fig~\ref{shot5}).
      
      \item Click and drag states across the screen for better visualisation.
      
      \item Click and fix position of multiple states on screen, using the
      \texttt{Ctrl+click} modifier key.
      
      \item Zoom in and out with mouse scroll wheel.
      
      \item Zoom in and out by clicking and dragging the right mouse button.
      
      \item Fit the complete machine in the viewable area by right-clicking
      anywhere on the white space.
    \end{enumerate}
  \end{description}
  
  \begin{figure}[!ht]
    \subfigure[Welcome Screen with Main Menu]{\label{shot1}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot1.png}
        }
    }
    \qquad\qquad
    \subfigure[Open File Dialog]{\label{shot2}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot2.png}
        }
    }
%     \caption{Open Dialog and Main Display of \dragon.}
%    \end{figure}
%   
%   \begin{figure}[!h]
    \subfigure[The Display \& Saved Dialog]{\label{shot3}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot3.png}
        }
    }
    \qquad\qquad
    \subfigure[Highlighted Neighbours]{\label{shot4}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot4.png}
        }
    }
    \caption{File Open, Save and Main Display of \dragon.}
   \end{figure}
   % >>>>>>>>>>>>>>>>>>> Another set of figures >>>>>>>>>>>>>>>>>>>.
   \begin{figure}[!ht]
    \subfigure[Selection Rectangle]{\label{shot5}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot5.png}
        }
    }
    \qquad\qquad
    \subfigure[Parser Error]{\label{shot8}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot8.png}
        }
    }
%     \caption{Selection features and Parse Capabilities of \dragon.}
%    \end{figure}
%    
%    \begin{figure}[!h]
    \subfigure[State Context Menu]{\label{shot6}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot6.png}
        }
    }
    \qquad\qquad
    \subfigure[Transition Context Menu]{\label{shot7}
        \fbox{
        \includegraphics[width=0.4\textwidth,keepaspectratio=true]{screenshots/shot7.png}
        }
    }
    \caption{Selection, Parse and Edit capabilities of \dragon.}
   \end{figure}
   
  % ============================================================================
  % ============================================================================
  \section{Installation Instructions} \label{install_instructions}
   To run \dragon{} you need to have installed the Sun's
   Java\textsuperscript{\texttrademark} Runtime Environment 1.6 or higher. Copy
   paste the jar file of \dragon{} (dragonfish-v1.0.jar) to any directory of
   your choice. To start the system issue the following command from the
   terminal in Linux, or double click on the jar file in Windows Explorer in
   Microsoft Windows suite of operating systems.
   \begin{verbatim}
   $ java -jar dragonfish-v1.0.jar
   \end{verbatim}
   The main window of \dragon{} will then open up.
   
  % ============================================================================
  % ============================================================================
  \section{How to use \dragon} \label{usage_instructions}
  Once the system is up and running, you can make use of all the features as
  mentioned previously.
  \begin{description}
  \item[Open a file] To open a machine from dot file, use the Open menu option
  from the File menu, navigate to your desired file and then open it.
  \item[Visualise the machine] Once the machine has been displayed, you can
  click and drag and use all the features as mentioned above for all sorts of
  visualisations. Use selection rectangle to fix multiple states simultaneously,
  or use Ctrl+Click. Click and drag fixed states to reposition them on the
  screen. Clicking on white space detaches them from the screen and makes them
  free-floating again.
  \item[Adding States] To add new states to the system, right-click on an
  existing state in the system and select Add State. A new state will be added
  connected to the selected state with a default label equal to the current ID
  of the new state.
  \item[Renaming States] Right-click on the desired state and select Rename from
  the context menu, the label field will automatically turn into a text editor.
  Edit the text in the label and then either press Enter key or click anywhere
  else on the screen to commit the changes.
  \item[Adding Transitions] To add a transition from an existing state to
  another, right click on the start state and then select Add Transition from
  the menu, then click on the desired end state to complete the transition
  creation process. To continue adding transitions from the same start state to
  other states, keep on clicking on the target states without clicking anywhere
  on the white space. Clicking on the white space while creating transitions,
  returns the system to the normal mode from the transition creation mode.
  \item[Deleting States] To delete a state, again follow the same procedure to
  open a context menu on the selected state and then select Delete from the
  context menu.
  \item[Deleting Transitions] To delete a transition right-click on the
  transition when the mouse pointer becomes a hand shaped pointer and the select
  delete from the context menu.
  \item[Saving the machine]  To save the machine back to the dot file, click on
  the Save menu option under the File menu. A dialog box confirming the save
  will appear, which you can safely dismiss.
  \end{description}
   
  % ============================================================================
  % ============================================================================
 \bibliographystyle{plain}
 \bibliography{references2}

\end{document}
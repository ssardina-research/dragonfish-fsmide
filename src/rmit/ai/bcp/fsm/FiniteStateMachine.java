package rmit.ai.bcp.fsm;

import java.util.HashMap;

import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Schema;

/**
 * Currently (15 Apr 2010), this class is tightly coupled with the prefuse data
 * structure viz. prefuse.data.* classes. This class stores all the information
 * contained in a DOT file, translated into objects by the DOTParser. This class
 * specifies the Schema for the prefuse data members as well, which is basically
 * the specification of 'what data needs to be stored' within this FSM pertinent
 * to the prefuse graph objects
 * 
 * @author abhijeet
 */
public class FiniteStateMachine implements StateMachine {
    // A prefuse Graph data member
    private String                machineName;
    private Graph                 stateMachineGraph;
    private HashMap<String, Node> hashMapNodes;
    private int                   edgeCount    = 0;
    private int                   nodeCount    = 0;
    private String                fileName     = null;
    private String                filePath     = null;
    /**
     * This flag determines if the Model has been edited by some external method
     */
    private boolean               changed      = false;

    // STATIC Declarations
    /* Data field labels included in generated Graphs */
    /** The label field in nodes and edges */
    public static final String    LABEL        = "label";
    /** The id for a given node/edge */
    public static final String    ID           = "id";
    /** The initial state of a given machine. Many states can be initial */
    public static final String    INITIAL      = "initial";
    /** The final state of a given machine. This can be many in number */
    public static final String    FINAL        = "final";
    /** This specifies if a transition is legal or not */
    public static final String    LEGAL        = "legal";

    /** Node table schema used for generated Graphs */
    public static final Schema    LABEL_SCHEMA = new Schema();
    /*
     * static {
     * LABEL_SCHEMA.addColumn(LABEL, String.class, "");
     * }
     */
    // Instead of the above schema, which only defines the label column for the
    // tables, I should have a separate node schema and edge schema, and within
    // each schema define the properties of the columns. This would be some
    // thing like, a node table that consists of all the nodes, will have the
    // following attributes for each node: node_id, node_label and some other
    // attribute if required. Similarly for the edge_table in the graph, it will
    // have the following attributes for each of the edges, apart from the usual
    // things: edge_id, edge_label, edge_guard. The last bit a little custom to
    // the dot format we will be parsing, say .dot+, .dotp or dotx, a simple
    // extended format of DOT format, which allows for an extra token for edges,
    // called 'legal' simply defining if an edge has any guard on it. This value
    // can later be used in some computational capabilities that can provided to
    // this IDE in realising the outputs of a given FSM
    public static final Schema    NODE_SCHEMA  = new Schema();
    public static final Schema    EDGE_SCHEMA  = new Schema();
    static {
        NODE_SCHEMA.addColumn(ID, String.class, "");
        NODE_SCHEMA.addColumn(LABEL, String.class, "");
        NODE_SCHEMA.addColumn(INITIAL, boolean.class, false);
        NODE_SCHEMA.addColumn(FINAL, boolean.class, false);

        EDGE_SCHEMA.addColumn(LABEL, String.class, "");
        EDGE_SCHEMA.addColumn(ID, String.class, "");
        EDGE_SCHEMA.addColumn(LEGAL, String.class, "");
    }

    // Default Constructor
    /**
     * Creates a blank FiniteStateMachine and specifies whether the transitions
     * are directed or undirected
     * 
     * @param isDirected
     */
    public FiniteStateMachine(boolean isDirected) {
        // System.out.println("Directed Graph: " + isDirected);
        stateMachineGraph = new Graph(isDirected);
        hashMapNodes = new HashMap<String, Node>();
        machineName = FiniteStateMachine.LABEL;

        stateMachineGraph.getNodeTable().addColumns(NODE_SCHEMA);
        stateMachineGraph.getEdgeTable().addColumns(EDGE_SCHEMA);

        /*
         * {
         * System.out.println("Within constructor's testing block");
         * System.out.println(stateMachineGraph.getNodeTable());
         * System.out.println(stateMachineGraph.getEdgeTable());
         * System.out.println(NODE_SCHEMA);
         * System.out.println(EDGE_SCHEMA);
         * System.out.println("Getting Out of constructor's testing block");
         * }
         */

    }

    /*
     * (non-Javadoc)
     * @see rmit.ai.bcp.fsm.StateMachine#addNode(java.lang.String,
     * java.lang.String)
     */
    @Override
    public void addNode(String id, String label) {
        // TODO Auto-generated method stub
        Node nNode;

        if (!hashMapNodes.containsKey(id)) {
            nNode = stateMachineGraph.addNode();
            nNode.setString(ID, id);
            label = label.replace('"', ' ');
            label = label.trim();
            if (label.length() != 0) {
                nNode.setString(LABEL, label);
            } else {
                nNode.setString(LABEL, id);
            }

            hashMapNodes.put(id, nNode);
            nodeCount++;
        }

    }

    /*
     * (non-Javadoc)
     * @see rmit.ai.bcp.fsm.StateMachine#addEdge(java.lang.String,
     * java.lang.String, java.lang.String, boolean)
     */
    @Override
    // FIXME This is an inefficient implementation.
    public void addEdge(String sourceState, String targetState,
            String edgeLabel, boolean guard) {
        Node sNode;
        Node tNode;

        // Fetch start node from the hash map, if exists; else add
        if (!hashMapNodes.containsKey(sourceState)) {
            sNode = stateMachineGraph.addNode();
            sNode.setString(ID, sourceState);
            sNode.setString(LABEL, sourceState);
            hashMapNodes.put(sourceState, sNode);
        } else {
            sNode = hashMapNodes.get(sourceState);
        }

        // Fetch target node from the hash map, if exists; else add
        if (!hashMapNodes.containsKey(targetState)) {
            tNode = stateMachineGraph.addNode();
            tNode.setString(ID, targetState);
            tNode.setString(LABEL, targetState);
            hashMapNodes.put(targetState, tNode);
        } else {
            tNode = hashMapNodes.get(targetState);
        }

        Edge e = stateMachineGraph.addEdge(sNode, tNode);
        edgeCount++;
        // Strip the labels off quotes and trailing/leading spaces
        edgeLabel = edgeLabel.replace('"', ' ');
        edgeLabel.trim();
        e.setString(LABEL, edgeLabel);
        /*
         * {
         * System.out.println(edgeLabel + edgeCount + " " + e);
         * }
         */

        // TODO Remove this block once testing is finished
        /*
         * {
         * Node testNode1 = stateMachineGraph.addNode();
         * testNode1.setString(LABEL, "Test1");
         * Node testNode2 = stateMachineGraph.addNode();
         * testNode2.setString(LABEL, "Test2");
         * stateMachineGraph.addEdge(testNode1, tNode);
         * stateMachineGraph.addEdge(tNode, tNode);
         * stateMachineGraph.addEdge(tNode, testNode1);
         * stateMachineGraph.addEdge(sNode, testNode1);
         * stateMachineGraph.addEdge(testNode1, testNode2);
         * stateMachineGraph.addEdge(sNode, testNode2);
         * System.out.println("SourceNode: " + sNode + " Target: " + tNode);
         * System.out.println(sNode);
         * System.out.println(tNode);
         * System.out.println(testNode1);
         * System.out.println(testNode2);
         * }
         */

    }

    /**
     * Returns the populated {@link prefuse.data.Graph} contained within this
     * machine
     * 
     * @return Graph The prefuse Graph
     */
    public Graph getGraph() {
        /*
         * {
         * System.out.println(stateMachineGraph.getNodeTable());
         * System.out.println(stateMachineGraph.getEdgeTable());
         * }
         */
        return stateMachineGraph;
    }

    /**
     * Returns the string representation of this machine's name
     * 
     * @return the machineName
     */
    public String getMachineName() {
        return machineName;
    }

    /**
     * Set the name of the current machine
     * 
     * @param machineName
     *            the machineName to set
     */
    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    /**
     * This sets the status of the machine being edited. This is solely for the
     * purpose of allowing methods to set this flag to true when they edit this
     * machine, which can later be used by the closing event or save actions
     */
    public void setChanged() {
        changed = true;
    }

    /**
     * Returns the name of the file from which it was loaded
     * 
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * sets the file name of this machine from which it was loaded
     * 
     * @param fileName
     *            the fileName to set
     */
    public void setFileName(String fileName) {
        if (this.fileName == null) {
            this.fileName = fileName;
        }
    }

    /**
     * Returns the path to the parent directory which contains the file
     * containing this machine
     * 
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets the path to the directory that has the dot file corresponding to
     * this machine
     * 
     * @param filePath
     *            the filePath to set
     */
    public void setFilePath(String filePath) {
        if (this.filePath == null) {
            this.filePath = filePath;
        }
    }

    /**
     * Returns the edit status of the machine if set by
     * {@link FiniteStateMachine#setChanged()}
     * 
     * @return true if the machine was edited
     */
    public boolean hasChanged() {
        return changed;
    }
}

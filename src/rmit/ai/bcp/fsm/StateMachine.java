/**
 * 
 */
package rmit.ai.bcp.fsm;


/**
 * This is an interface for any StateMachine
 * @author Abhijeet Anand
 */
public interface StateMachine {
    /**
     * Adds an edge to the StateMachine
     * @param sourceState
     * @param targetState
     * @param edgeLabel
     * @param guard
     */
    public void addEdge(String sourceState, String targetState, String edgeLabel, boolean guard);
    /**
     * Adds a node to the StateMachine given an id and a label
     * @param id
     * @param label
     */
    public void addNode(String id, String label);

}

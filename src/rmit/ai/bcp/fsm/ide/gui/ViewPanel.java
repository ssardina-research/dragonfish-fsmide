/**
 * 
 */
package rmit.ai.bcp.fsm.ide.gui;

import java.awt.BorderLayout;
import java.awt.LayoutManager;

import javax.swing.JPanel;

import rmit.ai.bcp.fsm.ide.gui.viewers.DotViewer;

/**
 * This is the view panel which houses all the views associateed to a model.
 * This is extensible wrapper class around DotViewer and can have tabbed
 * interface or similar such interfaces in future, preventing the MainFrame's
 * code to change.
 * 
 * @author abhijeet
 */
public class ViewPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 9059023299939095580L;
    /**
     * The Content Panel. This IS A FUTURE EXTENSION
     */
    // private final JTabbedPane tabbedViewPanel;

    /**
     * Current Content Panel
     */
    private JPanel            contentPanel     = null;

    /**
     * 
     */
    public ViewPanel() {
        // TODO Auto-generated constructor stub
        super(new BorderLayout(1, 1));
    }

    /**
     * @param layout
     */
    public ViewPanel(LayoutManager layout) {
        super(layout);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param isDoubleBuffered
     */
    public ViewPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param layout
     * @param isDoubleBuffered
     */
    public ViewPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        // TODO Auto-generated constructor stub
    }

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Add a new DotViewer display to this
     * @param display
     */
    public void addDisplay(DotViewer display) {
        // this.add(display, BorderLayout.CENTER);
        if (contentPanel == null) {
            contentPanel = display;
            this.add(contentPanel, BorderLayout.CENTER);
            this.validate();
        } else {
            this.removeAll();
            contentPanel = display;
            this.add(contentPanel, BorderLayout.CENTER);
            this.revalidate();
        }
    }

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

package rmit.ai.bcp.fsm.ide.gui.viewers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

import javax.swing.JPanel;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.layout.Layout;
import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.activity.Activity;
import prefuse.controls.DragControl;
import prefuse.controls.FocusControl;
import prefuse.controls.NeighborHighlightControl;
import prefuse.controls.WheelZoomControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Graph;
import prefuse.data.Schema;
import prefuse.data.Table;
import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.tuple.DefaultTupleSet;
import prefuse.data.tuple.TupleSet;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.render.PolygonRenderer;
import prefuse.render.Renderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.PrefuseLib;
import prefuse.visual.DecoratorItem;
import prefuse.visual.VisualGraph;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.GraphConstants;
import rmit.ai.bcp.fsm.ide.IdeConstants;
import rmit.ai.bcp.fsm.ide.controllers.MainController;
import rmit.ai.bcp.fsm.ide.controllers.PopupMenuController;
import rmit.ai.bcp.fsm.ide.controllers.RubberBandSelect;
import rmit.ai.bcp.fsm.ide.gui.MainFrame;
import rmit.ai.bcp.fsm.ide.gui.ViewPanel;

/**
 * This is the actual class which creates a visualisation for a model using
 * prefuse visualisation library
 * 
 * @author abhijeet
 */
public class DotViewer extends JPanel {

    /**
     * 
     */
    private static final long   serialVersionUID      = -3363689989708709496L;

    /**
     * {@link Visualization} that is going to be used for viewing the graph
     */
    private Visualization       m_vis;
    /**
     * {@link Display} used for this Visualisation
     */
    private Display             m_display;
    /**
     * The Model in MVC. This is the {@link FiniteStateMachine} to be displayed.
     * Later this can be used
     * to manipulate the data contained within itself. This copy of the FSM is
     * also stored in {@link MainFrame}, {@link MainController}.
     */
    private FiniteStateMachine  loadedFsm;
    /**
     * The {@link LabelRenderer} for the Nodes
     */
    private LabelRenderer       m_nodeRenderer;
    /**
     * The {@link EdgeRenderer} for the edges
     */
    private EdgeRenderer        m_edgeRenderer;
    /**
     * The custom Edge Label Decorator Schema for all {@link DotViewer}s
     */
    private static final Schema EDGE_DECORATOR_SCHEMA = PrefuseLib
                                                              .getVisualItemSchema();
    /**
     * Static block to set default values of {@link EDGE_DECORATOR_SCHEMA}
     */
    static {
//        EDGE_DECORATOR_SCHEMA.setDefault(VisualItem.INTERACTIVE, true);
        EDGE_DECORATOR_SCHEMA.setDefault(VisualItem.TEXTCOLOR, ColorLib
                .gray(0));
        EDGE_DECORATOR_SCHEMA.setDefault(VisualItem.FONT, FontLib.getFont(
                "Tahoma", 10));
    }
    /**
     * The custom Node Decorator
     */
    private static final Schema NODE_DECORATOR_SCHEMA = PrefuseLib
                                                              .getVisualItemSchema();
    /**
     * Static block to set default values of {@link EDGE_DECORATOR_SCHEMA}
     */
    static {
        NODE_DECORATOR_SCHEMA.setDefault(VisualItem.INTERACTIVE, true);
        NODE_DECORATOR_SCHEMA.setDefault(VisualItem.TEXTCOLOR, ColorLib
                .gray(128));
        NODE_DECORATOR_SCHEMA.setDefault(VisualItem.FONT, FontLib.getFont(
                "Tahoma", 10));
    }

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */
    /**
     * Set label positions. Labels are assumed to be DecoratorItem instances,
     * decorating their respective nodes. The layout simply gets the bounds
     * of the decorated node and assigns the label coordinates to the center
     * of those bounds.
     */
    class LabelLayout2 extends Layout {
        public LabelLayout2(String group) {
            super(group);
        }

        @SuppressWarnings("unchecked")
        public void run(double frac) {
            Iterator iter = m_vis.items(m_group);
            while (iter.hasNext()) {
                DecoratorItem decorator = (DecoratorItem) iter.next();
                VisualItem decoratedItem = decorator.getDecoratedItem();
                Rectangle2D bounds = decoratedItem.getBounds();

                double x = bounds.getCenterX();
                double y = bounds.getCenterY();

                /*
                 * modification to move edge labels more to the arrow head
                 * double x2 = 0, y2 = 0;
                 * if (decoratedItem instanceof EdgeItem){
                 * VisualItem dest = ((EdgeItem)decoratedItem).getTargetItem();
                 * x2 = dest.getX();
                 * y2 = dest.getY();
                 * x = (x + x2) / 2;
                 * y = (y + y2) / 2;
                 * }
                 */

                setX(decorator, null, x);
                setY(decorator, null, y);
            }
        }
    } // end of inner class LabelLayout

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * Create a new DotViewer with only the model
     * 
     * @param fsm
     */
    public DotViewer(FiniteStateMachine fsm) {
        this(fsm, true);
    }

    /**
     * Create a new DotViewer and specify whether the graphics is double
     * buffered
     * 
     * @param fsm
     *            The {@link FiniteStateMachine} to be displayed
     * @param isDoubleBuffered
     */
    public DotViewer(FiniteStateMachine fsm, boolean isDoubleBuffered) {
        super(new BorderLayout(), isDoubleBuffered);
        loadedFsm = fsm;

        // create a new, empty visualization for our data
        m_vis = new Visualization();

        // --------------------------------------------------------------
        // Register the data with the visualisation
        // adds graph to visualisation and sets renderer label field
        setGraph(loadedFsm.getGraph(), loadedFsm.getMachineName());
        
        // --------------------------------------------------------------
        // Set up renderers
        setupRenderers();

        // fix selected focus nodes
        setupFocusGroup(Visualization.FOCUS_ITEMS);
        setupFocusGroup(IdeConstants.SELECTED);

        // --------------------------------------------------------------------
        // create actions to process the visual data
        setupActions();

        // --------------------------------------------------------------------
        // set up a display to show the visualization
        setupDisplay();

        // Setup the Selection Rectangle on this Display
        createSelectionRectangle();

        // --------------------------------------------------------------------
        // launch the visualization

        // now we run our action list
        m_vis.run("draw");

    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * This method has been provided to run the {@link Visualization}
     * externally, such that if there need be of refreshing the
     * Visual abstraction, it can be done without creating a new
     * {@link DotViewer} instance
     */
    public void runVisualisation() {
        m_vis.run("draw");
    }

    /**
     * This updates the underlying model used by this {@link DotViewer} and
     * rerun the Visualisation. This method has been provided in case one does
     * not need to create a new DotViewer for a new {@link FiniteStateMachine}
     * model, and reuse the existing DotViewer. Though this is not necessary
     * since in future, creating a new instance of DotViewer for every new
     * FiniteStateMachine would be more useful since they can then be
     * attached to a new tab within the {@link ViewPanel}.
     * 
     * @param newModel
     *            A new {@link FiniteStateMachine}
     */
    public void updateModel(FiniteStateMachine newModel) {
        this.loadedFsm = newModel;
        setGraph(loadedFsm.getGraph(), loadedFsm.getMachineName());
    }

    /**
     * Adds a popup menu controller to the view.
     * 
     * @param popup
     */
    public void addContextMenu(PopupMenuController popup) {
        m_display.addControlListener(popup);
        m_display.getTextEditor().addKeyListener(popup);
    }

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */
    /**
     * Returns the associated visualisation with this view, needed by
     * controllers to access the visual items needed to edit the displayed
     * machine
     * 
     * @return
     */
    public Visualization getVisualisation() {
        return m_vis;
    }

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * This setups the graph contained within the {@link FiniteStateMachine}
     * onto this Visualisation / Visual Abstraction of the Graph
     * 
     * @param g
     * @param label
     */
    private void setGraph(Graph g, String label) {
        /*// update labeling
        DefaultRendererFactory drf = (DefaultRendererFactory)
                m_vis.getRendererFactory();
        // ((LabelRenderer) drf.getDefaultRenderer()).setTextField(label);
        ((LabelRenderer) drf.getDefaultRenderer())
                .setTextField(FiniteStateMachine.LABEL);
        ((EdgeRenderer) drf.getDefaultEdgeRenderer())
                .setEdgeType(prefuse.Constants.EDGE_TYPE_CURVE);*/

        // update graph
        m_vis.removeGroup(GraphConstants.GRAPH);
        VisualGraph vg = m_vis.addGraph(GraphConstants.GRAPH, g);
        m_vis.setValue(GraphConstants.EDGES, null, VisualItem.INTERACTIVE,
                Boolean.TRUE);

        VisualItem f = (VisualItem) vg.getNode(0);
        m_vis.getGroup(Visualization.FOCUS_ITEMS).setTuple(f);
        f.setFixed(false);

    }

    /**
     * Setup the renderers that this visualisation will be using to render the
     * visual abstraction of nodes, labels and edges
     */
    private void setupRenderers() {
        m_nodeRenderer = new LabelRenderer(FiniteStateMachine.LABEL);
        m_nodeRenderer.setHorizontalAlignment(Constants.CENTER);
        m_nodeRenderer.setRoundedCorner(3, 3);
        m_nodeRenderer.setHorizontalPadding(3);
        // m_nodeRenderer.setTextField(FiniteStateMachine.LABEL);

        m_edgeRenderer = new EdgeRenderer();
        m_edgeRenderer.setEdgeType(Constants.EDGE_TYPE_CURVE);

        // Create a default renderer factory with nodeRenderer as default
        DefaultRendererFactory rf = new DefaultRendererFactory(m_nodeRenderer);
        rf.add(new InGroupPredicate(GraphConstants.EDGES), m_edgeRenderer);

        // Another way of adding renderers to the Renderer Factory
        // DefaultRendererFactory rf = new
        // DefaultRendererFactory(m_nodeRenderer,
        // m_edgeRenderer);

        rf.add(new InGroupPredicate(IdeConstants.EDGE_DECORATORS),
                new LabelRenderer(FiniteStateMachine.LABEL));
        /*rf.add(new InGroupPredicate(IdeConstants.NODE_DECORATORS),
                new LabelRenderer(FiniteStateMachine.LABEL));*/
        // EDGE_DECORATOR_SCHEMA.setDefault(VisualItem.TEXTCOLOR, ColorLib
        // .gray(36));
        
        m_vis.setRendererFactory(rf);
        
        /*m_vis.addDecorators(IdeConstants.NODE_DECORATORS, GraphConstants.NODES,
                NODE_DECORATOR_SCHEMA);*/
        m_vis.addDecorators(IdeConstants.EDGE_DECORATORS, GraphConstants.EDGES,
                EDGE_DECORATOR_SCHEMA);
    }

    /**
     * Setup the actions that will be applied to this visual abstraction
     */
    private void setupActions() {
        // Enabling the following filter, prevents the display of disconnected
        // nodes
        /*
         * int hops = 30;
         * final GraphDistanceFilter filter = new GraphDistanceFilter(
         * GraphConstants.GRAPH, hops);
         */

        ColorAction fill = new ColorAction(GraphConstants.NODES,
                VisualItem.FILLCOLOR, ColorLib.rgb(200, 200, 255));
        fill.add(VisualItem.FIXED, ColorLib.rgb(255, 100, 100));
        fill.add(VisualItem.HIGHLIGHT, ColorLib.rgb(255, 200, 125));

        ActionList draw = new ActionList();
        // draw.add(filter);
        draw.add(fill);
        draw.add(new ColorAction(GraphConstants.NODES, VisualItem.STROKECOLOR,
                0));
        draw.add(new ColorAction(GraphConstants.NODES, VisualItem.TEXTCOLOR,
                ColorLib.rgb(0,
                        0, 0)));
        draw.add(new ColorAction(GraphConstants.EDGES, VisualItem.FILLCOLOR,
                ColorLib
                        .gray(200)));
        draw.add(new ColorAction(GraphConstants.EDGES, VisualItem.STROKECOLOR,
                ColorLib
                        .gray(200)));

        ActionList animate = new ActionList(Activity.INFINITY);
        animate.add(new ForceDirectedLayout(GraphConstants.GRAPH));
        animate.add(fill);
        animate.add(new LabelLayout2(IdeConstants.EDGE_DECORATORS));
        animate.add(new RepaintAction());

        ActionList colour = new ActionList(Activity.INFINITY);

        // finally, we register our ActionList with the Visualization.
        // we can later execute our Actions by invoking a method on our
        // Visualization, using the name we've chosen below.
        m_vis.putAction("draw", draw);
        m_vis.putAction("layout", animate);
        m_vis.putAction("color", colour);
        m_vis.putAction("repaint", new RepaintAction());

        m_vis.runAfter("draw", "layout");
    }

    /**
     * Setup the display, and tweak its parameters
     */
    private void setupDisplay() {
        m_display = new Display(m_vis);
        m_display.setSize(600, 600);
        m_display.pan(350, 350);
        m_display.setForeground(Color.GRAY);
        m_display.setBackground(Color.WHITE);

        // main display controls
        m_display.addControlListener(new FocusControl(1));
        m_display.addControlListener(new DragControl());
        // IF PanControl is enabled, it interferes with the RubberBandSelection
        // m_display.addControlListener(new PanControl());
        m_display.addControlListener(new ZoomControl());
        m_display.addControlListener(new WheelZoomControl());
        m_display.addControlListener(new ZoomToFitControl());
        m_display.addControlListener(new NeighborHighlightControl());

        m_display.setForeground(Color.GRAY);
        m_display.setBackground(Color.WHITE);

        this.add(m_display);
    }

    /**
     * Generalise this as far as possible. Using this method it should
     * enable us to setup as many focus groups as possible, such as selected,
     * focus, etc.
     * 
     * @param group
     *            The Group Name to be setup
     */
    private void setupFocusGroup(String group) {
        TupleSet focusGroupSet = m_vis.getGroup(group);
        if (focusGroupSet == null) {
            focusGroupSet = new DefaultTupleSet();
            m_vis.addFocusGroup(group, focusGroupSet);
            focusGroupSet = m_vis.getGroup(group);
        }
        focusGroupSet.addTupleSetListener(new TupleSetListener() {
            public void tupleSetChanged(TupleSet ts, Tuple[] add, Tuple[] rem) {
                // do whatever you do with newly selected/deselected items
                for (int i = 0; i < rem.length; ++i) {
                    ((VisualItem) rem[i]).setFixed(false);
                    ((VisualItem) rem[i]).setHighlighted(false);
                }

                for (int i = 0; i < add.length; ++i) {
                    ((VisualItem) add[i]).setFixed(false);
                    ((VisualItem) add[i]).setFixed(true);
                    ((VisualItem) add[i]).setHighlighted(true);
                }

                if (ts.getTupleCount() == 0) {
                    ts.addTuple(rem[0]);
                    ((VisualItem) rem[0]).setFixed(false);
                }
                m_vis.run("draw");
            }
        });
    }

    private void createSelectionRectangle() {
        Table rubberBandTable = new Table();
        rubberBandTable.addColumn(VisualItem.POLYGON, float[].class);
        rubberBandTable.addRow();
        m_vis.add(IdeConstants.RUBBER_BAND, rubberBandTable);
        VisualItem rubberBand = (VisualItem) m_vis.getVisualGroup(
                IdeConstants.RUBBER_BAND)
                .tuples().next();
        rubberBand.set(VisualItem.POLYGON, new float[8]);
        rubberBand.setStrokeColor(ColorLib.color(ColorLib.getColor(255, 0, 0)));

        // render the rubber band with the default polygon renderer
        Renderer rubberBandRenderer = new PolygonRenderer(
                Constants.POLY_TYPE_LINE);
        ((DefaultRendererFactory) m_vis.getRendererFactory()).add(
                new InGroupPredicate(IdeConstants.RUBBER_BAND),
                rubberBandRenderer);

        // Link the rubber band control to the rubber band display object
        m_display.addControlListener(new RubberBandSelect(rubberBand));
    }

} // END Class Definition (DotViewer)

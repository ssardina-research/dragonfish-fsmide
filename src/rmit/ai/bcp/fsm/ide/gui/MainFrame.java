package rmit.ai.bcp.fsm.ide.gui;

import javax.swing.JFrame;

import prefuse.util.ui.UILib;
import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.ide.gui.menu.MainMenuBar;
import rmit.ai.bcp.fsm.ide.gui.viewers.DotViewer;

/**
 * This is the parent frame for all visual items and acts as a View in MVC
 * 
 * @author abhijeet
 */
public class MainFrame extends JFrame {

    /**
     * Serial ID - generated
     */
    private static final long  serialVersionUID = 2413480464919787211L;
    /**
     * Application Name
     */
    private final String       appName          = "DragonFish - Finite State Machine IDE";

    /**
     * The MenuBar to be added. This is to be passed to the controller as
     * well, or returned to it, if asked for
     */
    private MainMenuBar        menuBar          = new MainMenuBar();
    /**
     * The view frame / ViewPanel
     */
    private ViewPanel          viewPanel;
    /**
     * The underlying model used by this view (MVC)
     */
    private FiniteStateMachine loadedModel;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * The only constructor so far (16 Apr 2010, 5:54 am)
     */
    public MainFrame(FiniteStateMachine fsm, String label) {
        loadedModel = fsm;
        UILib.setPlatformLookAndFeel();
        this.initMenuBar();
        this.initViewPanel();
        // this.setViewPanelContent();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setJMenuBar(menuBar);
        this.setContentPane(viewPanel);
        this.pack();

        this.setTitle(label);
        this.setVisible(false);

        // FIXME Remove this, it creates NullPointerException. Put it somewhere
        // more relevant
        /*
         * mainFrame.addWindowListener(new WindowAdapter() {
         * public void windowActivated(WindowEvent e) {
         * viewFrame.m_vis.run("layout");
         * }
         * public void windowDeactivated(WindowEvent e) {
         * viewFrame.m_vis.cancel("layout");
         * }
         * });
         */

    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /**
     * Updates the view with a new viewer associated to a model
     * @param view
     */
    public void updateView(DotViewer view) {
        viewPanel.addDisplay(view);
        // viewPanel.revalidate();
        // this.validate();
        this.pack();
    }

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */
    /**
     * Returns the MainMenuBar of this frame 
     * @return MainMenuBar
     */
    public MainMenuBar getMainMenuBar() {
        return menuBar;
    }

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Sets the current model used by this view
     * @param fsm
     */
    public void setModel(FiniteStateMachine fsm) {
        if (fsm != null) {
            loadedModel = fsm;
            // setViewPanelContent();
            this.setTitle(loadedModel.getMachineName());
//            System.out.println("added model to view");
        }
    }

    /*
     * (non-Javadoc)
     * @see java.awt.Frame#setTitle(java.lang.String)
     */
    public void setTitle(String title) {
        super.setTitle(title + " | " + appName);
    }

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * 
     */
    private void initMenuBar() {
        menuBar = new MainMenuBar();
    }

    /**
     * 
     */
    private void initViewPanel() {
        viewPanel = new ViewPanel();
    }

    /*
     * private void setViewPanelContent() {
     * viewPanel.addDisplay(new DotViewer(loadedModel));
     * }
     */

}

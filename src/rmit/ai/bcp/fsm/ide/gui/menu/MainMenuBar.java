package rmit.ai.bcp.fsm.ide.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import rmit.ai.bcp.fsm.ide.gui.menu.actions.AboutMenuAction;

/**
 * This class creates a default Menu Bar for the application, with some default
 * key/action bindings. Menu Items requiring some external data sources need to
 * be initialised properly before using. For e.g., Save or Save As may need to
 * access the currently open document, hence would require access to some data
 * structure which keeps a record of this.
 * 
 * @author Abhijeet Anand
 */
public class MainMenuBar extends JMenuBar {
    /**
     * 
     */
    private static final long    serialVersionUID = -5650678802882851753L;
    // Menus
    /**
     * The main file menu with options such as:
     * New.., Open..,
     * Save, Save As..,
     * Export.., Import..,
     * Exit..
     */
    private JMenu                fileMenu;
    public static final String   FILE             = "File";
    // Menu Items under File Menu
    private JMenuItem            newFileMenuItem;
    public static final String   NEW_FILE         = "New Machine";
    private JMenuItem            openFileMenuItem;
    public static final String   OPEN_FILE        = "Open File...";
    private JMenuItem            saveFileMenuItem;
    public static final String   SAVE             = "Save";
    private JMenuItem            saveAsMenuItem;
    public static final String   SAVE_AS          = "Save As...";
    private JMenuItem            importMenuItem;
    public static final String   IMPORT           = "Import";
    private JMenuItem            exportMenuItem;
    public static final String   EXPORT           = "Export";
    private JMenuItem            closeMenuItem;
    public static final String   CLOSE            = "Close";
    private JMenuItem            quitMenuItem;
    public static final String   QUIT             = "Quit";
    /**
     * The edit menu, that may have options to edit the file, or graph objects
     */
    private JMenu                editMenu;
    public static final String   EDIT_MENU        = "Edit";
    // Menu Items under Edit Menu
    private JMenuItem            editFileMenuItem;
    public static final String   EDIT_DOT         = "Edit DOT";
    private JMenuItem            editNodeMenuItem;
    public static final String   EDIT_NODE        = "Edit Node";
    private JMenuItem            editEdgeMenuItem;
    public static final String   EDIT_EDGE        = "Edit Edge";
    /**
     * The view menu, that may give access to open tabs and to switch between
     * them
     */
    private JMenu                viewMenu;
    public static final String   VIEW             = "View";
    // Menu Items under View Menu
    private ArrayList<JMenuItem> openTabsMenuItems;
    /**
     * This is a simple help menu giving access to help and about info
     */
    private JMenu                helpMenu;
    public static final String   HELP             = "Help";
    // Menu Items under Help Menu
    private JMenuItem            helpMenuItem;
    private JMenuItem            aboutMenuItem;
    public static final String   ABOUT            = "About";

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * The default constructor to the MenuBar, initialising components in a
     * default manner.
     */
    public MainMenuBar() {
        initFileMenu();
        initEditMenu();
        initViewMenu();
        initHelpMenu();
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * This method will add a menu item in the View menu, to switch between open
     * tabs. The signature of this method may change as required.
     * 
     * @param tabName
     * @param tabNumber
     * @return
     */
    public boolean addOpenTabMenuItem(String tabName, int tabNumber) {
        boolean added = false;
        // Add this tab to the list and create a link to show it
        openTabsMenuItems.add(new JMenuItem(tabName));
        return added;
    }
    
    /**
     * Add a FileOpenAction listener.
     * @param fOpenAL
     */
    public void addFileOpenActionListener(ActionListener fOpenAL) {
        openFileMenuItem.addActionListener(fOpenAL);
    }
    
    /**
     * Add a save action listener.
     * @param saveAL
     */
    public void addSaveActionListener(ActionListener saveAL) {
        saveFileMenuItem.addActionListener(saveAL);
    }

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Initialise the File Menu and its children, then add it to the Menu Bar
     */
    private void initFileMenu() {
        // Instantiate Menu
        fileMenu = new JMenu(FILE);

        newFileMenuItem = new JMenuItem(NEW_FILE);
        fileMenu.add(newFileMenuItem);
        
        openFileMenuItem = new JMenuItem(OPEN_FILE);
        //openFileMenuItem.addActionListener(new FileOpenAction(OPEN_FILE));
        
        fileMenu.add(openFileMenuItem);

        fileMenu.addSeparator();

        saveFileMenuItem = new JMenuItem(SAVE);
        fileMenu.add(saveFileMenuItem);
        
        saveAsMenuItem = new JMenuItem(SAVE_AS);
        fileMenu.add(saveAsMenuItem);

        fileMenu.addSeparator();

        importMenuItem = new JMenuItem(IMPORT);
        fileMenu.add(importMenuItem);
        
        exportMenuItem = new JMenuItem(EXPORT);
        fileMenu.add(exportMenuItem);

        fileMenu.addSeparator();

        closeMenuItem = new JMenuItem(CLOSE);
        fileMenu.add(closeMenuItem);
        
        quitMenuItem = new JMenuItem(QUIT);
        // quitMenuItem.setMnemonic('Q');
        quitMenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                System.exit(0);
            }
        });
        
        fileMenu.add(quitMenuItem);

        // Add File Menu to the MenuBar
        add(fileMenu);

    }

    /**
     * Initialise the Edit Menu and its children, then add it to the Menu Bar
     */
    private void initEditMenu() {
        // Instantiate Menu
        editMenu = new JMenu(EDIT_MENU);

        editFileMenuItem = new JMenuItem(EDIT_DOT);
        editMenu.add(editFileMenuItem);

        editMenu.addSeparator();

        editNodeMenuItem = new JMenuItem(EDIT_NODE);
        editMenu.add(editNodeMenuItem);
        editEdgeMenuItem = new JMenuItem(EDIT_EDGE);
        editMenu.add(editEdgeMenuItem);

        // Add Edit Menu to the Menu Bar
        add(editMenu);

    }

    /**
     * Initialise the View Menu and its children, then add it to the Menu Bar
     */
    private void initViewMenu() {
        // Instantiate Menu
        viewMenu = new JMenu(VIEW);

        openTabsMenuItems = new ArrayList<JMenuItem>();

        // A dummy Item
        viewMenu.add("Currently open tabs").setEnabled(false);
        viewMenu.addSeparator();
        // Add the View Menu to the Menu Bar
        add(viewMenu);

    }

    /**
     * Initialise the Help Menu and its children, then add it to the Menu Bar
     */
    private void initHelpMenu() {
        // Instantiate Menu
        helpMenu = new JMenu(HELP);

        helpMenuItem = new JMenuItem(HELP);
        helpMenu.add(helpMenuItem);

        aboutMenuItem = new JMenuItem(ABOUT);
        aboutMenuItem.addActionListener(new AboutMenuAction());
        helpMenu.add(aboutMenuItem);

        // Add the Help Menu to the Menu Bar
        add(helpMenu);

    }

}

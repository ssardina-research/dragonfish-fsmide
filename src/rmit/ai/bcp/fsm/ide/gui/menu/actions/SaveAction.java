/**
 * 
 */
package rmit.ai.bcp.fsm.ide.gui.menu.actions;

import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JOptionPane;

import prefuse.data.Node;
import prefuse.data.Tuple;
import prefuse.data.tuple.TupleSet;
import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.ide.controllers.MainController;

/**
 * This class handles the save action
 * @author abhijeet
 */
public class SaveAction extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = -2537773490159190554L;
    /**
     * The main controller
     */
    private MainController    m_controller;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * 
     */
    public SaveAction(MainController controller) {
        // TODO Auto-generated constructor stub
        m_controller = controller;
    }

    /**
     * @param name
     */
    public SaveAction(MainController controller, String name) {
        super(name);
        // TODO Auto-generated constructor stub
        m_controller = controller;
    }

    /**
     * @param name
     * @param icon
     */
    public SaveAction(MainController controller, String name, Icon icon) {
        super(name, icon);
        // TODO Auto-generated constructor stub
        m_controller = controller;
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */
    /*
     * (non-Javadoc)
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        FiniteStateMachine saveModel = (FiniteStateMachine) m_controller
                .getModel();
        if (saveModel != null && saveModel.getFilePath() != null
                && saveModel.getFileName() != null) {
            /*
             * Only the underlying model is needed because, while editing the
             * Visualisation in the editor, it directly modifies the underlying
             * model's Graph object containing the actual data. Traversing
             * through
             * the Graph's backing data table of nodes and edges should be
             * enough to
             * save the information in the file.
             */
            String filePath = saveModel.getFilePath() + "/" + saveModel.getFileName();
            File saveFile = new File(filePath);
            // use buffering
            Writer output = null;
            try {
                boolean graphIsDirected = saveModel.getGraph().isDirected();
                TupleSet nodesTupleSet = saveModel.getGraph().getNodes();
                TupleSet edgesTupleSet = saveModel.getGraph().getEdges();
                Iterator<Node> nodesIterator = nodesTupleSet.tuples();
                Iterator<Tuple> edgesIterator = edgesTupleSet.tuples();
                String graphTypeName;
                String edgeOp;
                if (graphIsDirected) {
                    graphTypeName = "digraph ";
                    edgeOp = " -> ";
                } else {
                    graphTypeName = "graph ";
                    edgeOp = " -- ";
                }
                graphTypeName += saveModel.getMachineName() + " {\n";

                // Begin Writing to file
                output = new BufferedWriter(new FileWriter(saveFile));
                // Write the First line
                output.write(graphTypeName);
                // Write all the nodes
                while (nodesIterator.hasNext()) {
                    Tuple nextNode = nodesIterator.next();
                    String nodeId = (String) nextNode.get(0);
                    if (nodeId.length() > 0) {
                        output.write(nodeId + " [label = \""
                                + nextNode.get(1) + "\"];\n");
                    }
                }
                // Write all the edges
                while (edgesIterator.hasNext()) {
                    Tuple nextEdge = edgesIterator.next();
                    output.write(nextEdge.get(0) + edgeOp + nextEdge.get(1)
                            + " [label = \""
                            + nextEdge.get(2).toString().trim() + "\"];\n");
                }
                output.write("}\n");

                output.close();
                // Show Save success message
                String successMessage = "\"" + saveModel.getFileName() + "\" saved successfully";
                JOptionPane.showMessageDialog(null,
                        successMessage, "File Saved",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

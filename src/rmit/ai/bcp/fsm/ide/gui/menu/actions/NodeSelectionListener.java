/**
 * 
 */
package rmit.ai.bcp.fsm.ide.gui.menu.actions;

import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.tuple.TupleSet;
import prefuse.visual.VisualItem;

/**
 * This class is simply a stub currently, and should later be modified in
 * conjunction with RubberBandSelect, such that when a selection action is
 * performed, this listener listens to it and performs some actions o the
 * selected nodes, which were selected by the RubberBandSelect
 * 
 * @author abhijeet
 */
public class NodeSelectionListener implements TupleSetListener {

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * 
     */
    public NodeSelectionListener() {
        // TODO Auto-generated constructor stub
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */
    /*
     * (non-Javadoc)
     * @see
     * prefuse.data.event.TupleSetListener#tupleSetChanged(prefuse.data.tuple
     * .TupleSet, prefuse.data.Tuple[], prefuse.data.Tuple[])
     */
    @Override
    public void tupleSetChanged(TupleSet tset, Tuple[] added, Tuple[] removed) {
        // TODO Auto-generated method stub
        // do whatever you do with newly selected/deselected items
        // This method should also update the selected nodes list somewhere in
        // the controller.
        // m_vis.cancel("layout");
        for (int i = 0; i < added.length; i++) {
            VisualItem item = (VisualItem) added[i];
            item.setHighlighted(true);
        }
        for (int i = 0; i < removed.length; i++) {
            VisualItem item = (VisualItem) removed[i];
            item.setHighlighted(false);
        }

    }

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

package rmit.ai.bcp.fsm.ide.gui.menu.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.ide.controllers.MainController;
import rmit.ai.bcp.fsm.ide.loaders.DotLoader;
import rmit.ai.bcp.fsm.ide.utils.FileExtensionFilter;

/**
 * This class extends the {@link AbstractAction} class implementing the
 * {@link ActionListener} Interface. This class will handle the Open Menu
 * Action. It
 * will display a proper File Chooser and then will send the selected file to
 * the appropriate file loader
 * 
 * @author Abhijeet Anand
 */
public class FileOpenAction extends AbstractAction {

    /**
     * Generated Serial Version ID for this class
     */
    private static final long serialVersionUID = 4609986834964215074L;
    // File Chooser to select a file to be displayed and parsed
    // private static String dotFileDescription = "";
    // private static String dotFileExtension = ".dot";
    private JFileChooser      dotFileChooser;
    // Present Working Directory
    private String            pwd              = null;
    /**
     * The Main Controller
     */
    private MainController    m_controller;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    public FileOpenAction(MainController controller) {
        m_controller = controller;
        // Set the File Choosers opening directory to user's pwd
        dotFileChooser = new JFileChooser(System.getProperty("user.dir"));
        // Set an extension filter to the file chooser
        dotFileChooser.setFileFilter(new FileExtensionFilter("dot",
                "*.dot (DOT Graph Files)"));
        dotFileChooser.setAcceptAllFileFilterUsed(false);
    }

    public FileOpenAction(MainController controller, String name) {
        super(name);
        m_controller = controller;
        // Set the File Choosers opening directory to user's pwd
        dotFileChooser = new JFileChooser(System.getProperty("user.dir"));
        // Set an extension filter to the file chooser
        dotFileChooser.setFileFilter(new FileExtensionFilter("dot",
                "*.dot (DOT Graph Files)"));
        dotFileChooser.setAcceptAllFileFilterUsed(false);
    }

    public FileOpenAction(MainController controller, String name, Icon icon) {
        super(name, icon);
        m_controller = controller;
        // Set the File Choosers opening directory to user's pwd
        dotFileChooser = new JFileChooser(System.getProperty("user.dir"));
        // Set an extension filter to the file chooser
        dotFileChooser.setFileFilter(new FileExtensionFilter("dot",
                "*.dot (DOT Graph Files)"));
        dotFileChooser.setAcceptAllFileFilterUsed(false);
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        File dataFile = null;
        if (pwd != null) {
            dotFileChooser.setCurrentDirectory(new File(pwd));
        }
        boolean loaded = false;

        while (!loaded) {
            int action = dotFileChooser.showOpenDialog(null);
            // If a file was selected
            if (action == JFileChooser.APPROVE_OPTION) {
                dataFile = dotFileChooser.getSelectedFile();
                // gets the path to parent without trailing slash
                pwd = dataFile.getParent();

                // What to do next.. Should I call DotViewer from within the
                // DotLoader or shall I create a DotViewer here and then pass it
                // onto some mechanism
                // EDIT 1: instead send this to the controller
                // Now that we have a File, lets call a dot loader and send its
                // output to the controller
                FiniteStateMachine fsm = DotLoader.loadFile(dataFile);
                dataFile = null;
                if (fsm != null) {
                    m_controller.setModel(fsm);
                    loaded ^= true;
                } else {
                    JOptionPane
                            .showMessageDialog(
                                    null,
                                    "Error parsing the file. Kindly check that its in correct format. \n"
                                            + DotLoader.errorMessage(),
                                    "Parse Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                loaded ^= true;
            }
        }

    }

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

package rmit.ai.bcp.fsm.ide.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.ide.parser.dotparser.DOTParser;
import rmit.ai.bcp.fsm.ide.parser.dotparser.ParseException;

/**
 * This is a wrapper class around DOTParser which provides a factory method to
 * call the parser and compile the machine from dot file.
 * 
 * @author abhijeet
 */
public class DotLoader {
    // private FiniteStateMachine fsm;
    private static String parseErrorMessage;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Static factory method to parse the given dot file
     * @param dotFile
     * @return The FiniteStateMachine loaded by the parser.
     */
    public static FiniteStateMachine loadFile(File dotFile) {
        FiniteStateMachine fsm = null;
        try {
            FileInputStream dotFileStream = new FileInputStream(dotFile);
            DOTParser parser = new DOTParser(dotFileStream);
            try {
                fsm = parser.createFSM();
                fsm.setFileName(dotFile.getName());
                fsm.setFilePath(dotFile.getParent());
            } catch (IOException e) {
                parseErrorMessage = e.getMessage();
            } catch (ParseException e) {
                parseErrorMessage = e.getMessage();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fsm;
    }

    public static String errorMessage() {
        return parseErrorMessage;
    }

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

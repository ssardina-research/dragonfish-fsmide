/**
 * 
 */
package rmit.ai.bcp.fsm.ide.controllers;

import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.StateMachine;
import rmit.ai.bcp.fsm.ide.gui.MainFrame;
import rmit.ai.bcp.fsm.ide.gui.menu.actions.FileOpenAction;
import rmit.ai.bcp.fsm.ide.gui.menu.actions.SaveAction;
import rmit.ai.bcp.fsm.ide.gui.viewers.DotViewer;

/**
 * This is the main controller of the application, controlling and coordinating
 * all the other controllers attached to the GUI. The other controllers request
 * the model contained with this controller and modify it accordingly if needed.
 * 
 * @author abhijeet
 */
public class MainController {
    /**
     * The Model used by this controller
     */
    private FiniteStateMachine  model  = null;
    /**
     * The View controlled by this controller
     */
    private MainFrame           view   = null;

    // Other Controllers
    /**
     * File Open Controller
     */
    private FileOpenAction      fOpenAction;
    /**
     * Save Action
     */
    private SaveAction          saveAction;

    /**
     * Popup Menu Controller
     */
    private PopupMenuController popupAction;
    /**
     * This stores the Display ({@link DotViewer}) for a given Model
     * ({@link FiniteStateMachine}). This should later be changed to something
     * of the sort of {@link ArrayList}, such that a list of all attached
     * {@link Viewers} can be stored and assigned to separate
     * {@link DisplayPanel}s within the {@link ViewPanel} or {@link MainFrame}.
     */
    private DotViewer           viewer = null;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * Creates a new controller, with specified model and view
     * 
     * @param fsmModel
     * @param fsmView
     */
    public MainController(FiniteStateMachine fsmModel, MainFrame fsmView) {
        model = fsmModel;
        view = fsmView;
        initMenuConnections();
        viewer = new DotViewer(model);
        this.addContextMenu(viewer);
        view.updateView(viewer);
        view.setVisible(false);
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */
    /**
     * Returns the associated model to this controller
     * 
     * @return StateMachine Abstract machine
     */
    public StateMachine getModel() {
        return model;
    }

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Attach the model to this controller. This also alters the view and
     * updates it. But before changing the model it should also check if the
     * current model has been changed, in such case should attempt to save the
     * model.
     * 
     * @param fsm
     */
    public void setModel(FiniteStateMachine fsm) {
        if (fsm != null) {
            model = fsm;
            view.setModel(model);
            viewer = new DotViewer(model);
            this.addContextMenu(viewer);
            view.updateView(viewer);
            // --------------------- Alternate Way -----------------------
            /*
             * This alternate view does not create a new instance of viewer,
             * instead simply updates the model contained in the viewer, and
             * asks the viewer to rerun the visualisation. But then this also
             * prevents the Viewer from setting up renderers and other related
             * visualisation parameters on the new model. Hence the above method
             * is more reasonable and logical.
             */
            {
                // viewer.updateModel(model);
                // reattaches the context menu to the new data, such that
                // correct data is manipulated
                // this.addContextMenu(viewer);
                // viewer.runVisualisation(); // Not Required.
                System.gc();
            }
        }
    }

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Initialises the menu connections
     */
    private void initMenuConnections() {
        fOpenAction = new FileOpenAction(this);
        view.getMainMenuBar().addFileOpenActionListener(fOpenAction);
        saveAction = new SaveAction(this);
        view.getMainMenuBar().addSaveActionListener(saveAction);
    }

    /**
     * Adds a context menu to the viewer
     * 
     * @param viewer
     */
    private void addContextMenu(DotViewer viewer) {
        popupAction = new PopupMenuController(viewer.getVisualisation());
        viewer.addContextMenu(popupAction);
    }

}

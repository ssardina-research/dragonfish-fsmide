/**
 * 
 */
package rmit.ai.bcp.fsm.ide.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.controls.ControlAdapter;
import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.visual.AggregateItem;
import prefuse.visual.EdgeItem;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;
import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.GraphConstants;

/**
 * This is the controller that performs all the main editing actions of the IDE.
 * Currently this controller is only associated with a pop-up menu contained
 * within itself. This is though a bad design since it has the View component
 * within itself as well.
 * 
 * @author abhijeet
 * 
 * Adapted from an example of GraphEditor:
 * Demonstration of graph editor functionality.
 * See https://sourceforge.net/forum/forum.php?thread_id=1597565&forum_id=343013
 * for a discussion about the rubberband/drag select/multiple selection of
 * nodes, while the following thread 
 * https://sourceforge.net/forum/message.php?msg_id=3758973&forum_id=343013
 * contains the discussion about drawing edges interactively.
 *
 * @author <a href="http://jheer.org">jeffrey heer</a>
 * @author Aaron Barsky
 * @author Bj�rn Kruse
 */
public class PopupMenuController extends ControlAdapter implements
        ActionListener {

    private Graph         g;
    private Display       d;
    private Visualization vis;
    private VisualItem    clickedItem;

    /**
     * Popup Menu activated when clicked on a node
     */
    private JPopupMenu    nodePopupMenu;
    /**
     * Popup Menu activated when clicked on an edge
     */
    private JPopupMenu  edgePopupMenu;
//    private JPopupMenu    popupMenu;

    private Point2D       mousePosition = new Point2D.Double();
    private VisualItem    nodeVisualDummy;
    public Node           nodeSourceDummy;
    public Edge           edgeDummy;
    private boolean       creatingEdge  = false;
    private boolean       editing;

    /**
     * Action ID
     */
    // private int action = 1;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */

    /**
     * Creates a new PopupMenuController attached to this Visualisation
     * 
     * @param vis
     *            The {@link Visualization} to be attached
     */
    public PopupMenuController(Visualization vis) {
        this.vis = vis;
        this.g = (Graph) vis.getSourceData(GraphConstants.GRAPH);
        this.d = vis.getDisplay(0);

        createDummy();

        // create popupMenu for nodes and edges
        nodePopupMenu = new JPopupMenu();
        JMenuItem stateMenuHeader = new JMenuItem("State Menu");
        stateMenuHeader.setEnabled(false);
        edgePopupMenu = new JPopupMenu();
        JMenuItem edgeMenuHeader = new JMenuItem("Transition Menu");
        edgeMenuHeader.setEnabled(false);

        JMenuItem delete = new JMenuItem("Delete", 'd');
        JMenuItem editText = new JMenuItem("Rename", 'r');
        JMenuItem addEdge = new JMenuItem("Add Transition", 't');
        JMenuItem addNode = new JMenuItem("Add State", 's');
        
        JMenuItem deleteTransition = new JMenuItem("Delete", KeyEvent.VK_D);

        delete.setActionCommand("node_delete");
        editText.setActionCommand("node_editText");
        addEdge.setActionCommand("node_addEdge");
        addNode.setActionCommand("node_addNode");
        
        deleteTransition.setActionCommand("edge_delete");

        nodePopupMenu.addSeparator();
        nodePopupMenu.add(stateMenuHeader);
        nodePopupMenu.addSeparator();
        nodePopupMenu.add(delete);
        nodePopupMenu.addSeparator();
        nodePopupMenu.add(editText);
        nodePopupMenu.addSeparator();
        nodePopupMenu.add(addNode);
        nodePopupMenu.add(addEdge);
        
        edgePopupMenu.addSeparator();
        edgePopupMenu.add(edgeMenuHeader);
        edgePopupMenu.addSeparator();
        edgePopupMenu.add(deleteTransition);
//        edgePopupMenu.addSeparator();

        delete.setMnemonic(KeyEvent.VK_D);
        editText.setMnemonic(KeyEvent.VK_R);
        addEdge.setMnemonic(KeyEvent.VK_T);
        addNode.setMnemonic(KeyEvent.VK_S);

        delete.addActionListener(this);
        editText.addActionListener(this);
        addEdge.addActionListener(this);
        addNode.addActionListener(this);
        
        deleteTransition.addActionListener(this);

        // create popupMenu for 'background'
        /*popupMenu = new JPopupMenu();
        addNode = new JMenuItem("Add State", 's');
        addNode.setActionCommand("addNode");
        popupMenu.addSeparator();
        popupMenu.add(addNode);
        addNode.setMnemonic(KeyEvent.VK_S);
        addNode.addActionListener(this);*/

    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */
    /**
     * called on popupMenu Action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().startsWith("node")) {
            if (e.getActionCommand().endsWith("delete")) {
                g.removeNode((Node) clickedItem.getSourceTuple());
            } else if (e.getActionCommand().endsWith("editText")) {
                startEditing(clickedItem);
            } else if (e.getActionCommand().endsWith("addEdge")) {
                creatingEdge = true;
                createTemporaryEdgeFromSourceToDummy(clickedItem);
            } else if (e.getActionCommand().endsWith("addNode")) {
                addNewNode(clickedItem);
            }
        } else if (e.getActionCommand().startsWith("edge")) {
            if (e.getActionCommand().endsWith("delete")) {
                g.removeEdge((Edge) clickedItem.getSourceTuple());
            }
        } else {
            if (e.getActionCommand().equals("addNode")) {
                int node = (int) (Math.random() * (g.getNodeCount() - 1));
                Node source = g.getNode(node); // random source
                addNewNode(source);
            } else {

            }
        }
    }

    @Override
    public void itemKeyReleased(VisualItem item, KeyEvent e) {
        keyReleased(e);
    }

    /**
     * only necessary if edge-creation is used together with aggregates and
     * the edge should move on when mousepointer moves within an aggregate
     */
    @Override
    public void itemMoved(VisualItem item, MouseEvent e) {
        if (item instanceof AggregateItem)
            mouseMoved(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // called, when keyReleased events on displays textEditor are fired
        if (e.getKeyCode() == KeyEvent.VK_ENTER && editing) {
            stopEditing();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (creatingEdge) {
            stopEdgeCreation();
            return;
        } else if (editing) {
            stopEditing();
        }
        if (SwingUtilities.isRightMouseButton(e)) {
            clickedItem = null;
            if (creatingEdge)
                stopEdgeCreation();
//            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // necessary, if you have no dummy and this ControlAdapter is running
        if (nodeVisualDummy == null)
            return;
        // update the coordinates of the dummy-node to the mouselocation so the
        // tempEdge is drawn to the mouselocation too
        d.getAbsoluteCoordinate(e.getPoint(), mousePosition);
        nodeVisualDummy.setX(mousePosition.getX());
        nodeVisualDummy.setY(mousePosition.getY());
    }
    
    @Override
    public void itemClicked(VisualItem item, MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            clickedItem = item;
            // on rightclick, stop the edge creation
            if (creatingEdge) {
                stopEdgeCreation();
                return;
            }

            if (item instanceof NodeItem) {
                nodePopupMenu.show(e.getComponent(), e.getX(), e.getY());
            } else if (item instanceof EdgeItem) {
                edgePopupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        } else if (SwingUtilities.isLeftMouseButton(e)) {
            if (creatingEdge) {
                g.addEdge(edgeDummy.getSourceNode(), (Node) item
                        .getSourceTuple());
            } else if (item instanceof NodeItem) { // a node was clicked
                /*
                 * switch (action) {
                 * case 1: // create a node
                 * addNewNode(item);
                 * break;
                 * case 2: // create an edge
                 * creatingEdge = true;
                 * createTemporaryEdgeFromSourceToDummy(item);
                 * break;
                 * case 3: // rename node
                 * startEditing(item);
                 * break;
                 * default:
                 * break;
                 * }
                 */

            }
        }
    }

    

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Removes all dummies, the node and the two edges. Additionally sets the
     * variables who stored a reference to these dummies to null.
     */
    @SuppressWarnings("unused")
    private void removeAllDummies() {
        if (nodeSourceDummy != null)
            g.removeNode(nodeSourceDummy);
        edgeDummy = null;
        nodeSourceDummy = null;
        nodeVisualDummy = null;
    }

    @SuppressWarnings("unused")
    private void removeNodeDummy() {
        g.removeNode(nodeSourceDummy);
        nodeSourceDummy = null;
        nodeVisualDummy = null;
    }
    
    private void startEditing(VisualItem item) {
        editing = true;
        // d.editText(item, VisualItem.LABEL);
        d.editText(item, FiniteStateMachine.LABEL);
//        item.setString(FiniteStateMachine.LABEL, (String) item.get(FiniteStateMachine.LABEL));
    }

    // ---------------------------------------------
    // --- methods for edgeCreation
    // ---------------------------------------------

    private void stopEdgeCreation() {
        creatingEdge = false;
        removeEdgeDummy();
    }

    private void stopEditing() {
        editing = false;
        d.stopEditing();
    }

    private void addNewNode(Node source) {
        Node n = g.addNode(); // create a new node
        // n.set(VisualItem.LABEL, "Node " + n.getRow());
        // Assign the Node ID
        n.set(FiniteStateMachine.ID, Integer.toString(n.getRow()));
        // assign a new name
        n.set(FiniteStateMachine.LABEL, "Node " + n.getRow());
        // add an edge from source to the new node
        g.addEdge(source, n);
    }

    private void addNewNode(VisualItem source) {
        addNewNode((Node) source.getSourceTuple());
    }

    /**
     * Removes all edge dummies, if the references stored to these dummies are
     * not null. Additionally sets the references to these dummies to null.
     */
    private void removeEdgeDummy() {
        if (edgeDummy != null) {
            g.removeEdge(edgeDummy);
            edgeDummy = null;
        }
    }
    
    private VisualItem createDummy() {
        // create the dummy node for the creatingEdge mode
        nodeSourceDummy = g.addNode();
        // nodeSourceDummy.set(VisualItem.LABEL, "");
        nodeSourceDummy.set(FiniteStateMachine.LABEL, "");

        nodeVisualDummy = vis.getVisualItem(GraphConstants.NODES,
                nodeSourceDummy);
        nodeVisualDummy.setSize(0.0);
        nodeVisualDummy.setVisible(false);

        /*
         * initially set the dummy's location. upon mouseMove events, we
         * will do that there. otherwise, the dummy would appear on top
         * left position until the mouse moves
         */
        double x = d.getBounds().getCenterX();
        double y = d.getBounds().getCenterY();
        mousePosition.setLocation(x, y);
        nodeVisualDummy.setX(mousePosition.getX());
        nodeVisualDummy.setY(mousePosition.getY());
        return nodeVisualDummy;
    }

    private void createTemporaryEdgeFromDummyToTarget(Node target) {
        if (edgeDummy == null) {
            edgeDummy = g.addEdge((Node) nodeVisualDummy.getSourceTuple(),
                    target);
        }
    }

    /**
     * @param target
     *            the item to use as target for the dummy edge
     */
    @SuppressWarnings("unused")
    private void createTemporaryEdgeFromDummyToTarget(VisualItem target) {
        createTemporaryEdgeFromDummyToTarget((Node) target.getSourceTuple());
    }

    private void createTemporaryEdgeFromSourceToDummy(Node source) {
        if (edgeDummy == null) {
            edgeDummy = g.addEdge(source, nodeSourceDummy);
        }
    }

    /**
     * @param source
     *            the item to use as source for the dummy edge
     */
    private void createTemporaryEdgeFromSourceToDummy(VisualItem source) {
        createTemporaryEdgeFromSourceToDummy((Node) source.getSourceTuple());
    }

}

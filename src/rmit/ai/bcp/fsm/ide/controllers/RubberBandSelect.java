/**
 * 
 */
package rmit.ai.bcp.fsm.ide.controllers;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

import javax.swing.SwingUtilities;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.controls.ControlAdapter;
import prefuse.data.Tuple;
import prefuse.data.tuple.TupleSet;
import prefuse.visual.VisualItem;
import rmit.ai.bcp.fsm.GraphConstants;
import rmit.ai.bcp.fsm.ide.IdeConstants;

/**
 * RubberBandSelect creates a Selection Rectangle around nodes and performs the
 * actual selection logic of nodes. These selected nodes can then be processed
 * later for other actions.
 * This class has been adapted from <a
 * href=https://sourceforge.net/forum/forum.php?thread_id=1597565&forum_id=343013>RubberBandSelection</a>
 * for a discussion about the rubberband/drag select/multiple selection of
 * nodes
 * 
 * @author Abhijeet Anand
 * @author <a href="http://jheer.org">jeffrey heer</a>
 * @author Aaron Barsky
 * @author Bj�rn Kruse
 */
public class RubberBandSelect extends ControlAdapter {
    private int        downX1, downY1;
    private VisualItem rubberBand;
    Point2D            screenPoint = new Point2D.Float();
    Point2D            absPoint    = new Point2D.Float();
    Rectangle2D        rect        = new Rectangle2D.Float();
    Rectangle          r           = new Rectangle();

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * 
     */
    public RubberBandSelect(VisualItem rubberBand) {
        this.rubberBand = rubberBand;
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */
    public void mousePressed(MouseEvent e) {
        if (!SwingUtilities.isLeftMouseButton(e))
            return;

        Display d = (Display) e.getComponent();
        Visualization vis = d.getVisualization();
        TupleSet focus = vis.getFocusGroup(IdeConstants.SELECTED);
        if (!e.isShiftDown()) {
            focus.clear();
        }

        float[] bandRect = (float[]) rubberBand.get(VisualItem.POLYGON);
        bandRect[0] = bandRect[1] = bandRect[2] = bandRect[3] =
                bandRect[4] = bandRect[5] = bandRect[6] = bandRect[7] = 0;

        d.setHighQuality(false);
        screenPoint.setLocation(e.getX(), e.getY());
        d.getAbsoluteCoordinate(screenPoint, absPoint);
        downX1 = (int) absPoint.getX();
        downY1 = (int) absPoint.getY();
        rubberBand.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    public void mouseDragged(MouseEvent e) {
        if (!SwingUtilities.isLeftMouseButton(e))
            return;

        Display d = (Display) e.getComponent();
        screenPoint.setLocation(e.getX(), e.getY());
        d.getAbsoluteCoordinate(screenPoint, absPoint);
        int x1 = downX1;
        int y1 = downY1;
        int x2 = (int) absPoint.getX();
        int y2 = (int) absPoint.getY();

        float[] bandRect = (float[]) rubberBand.get(VisualItem.POLYGON);
        bandRect[0] = x1;
        bandRect[1] = y1;
        bandRect[2] = x2;
        bandRect[3] = y1;
        bandRect[4] = x2;
        bandRect[5] = y2;
        bandRect[6] = x1;
        bandRect[7] = y2;

        if (x2 < x1) {
            int temp = x2;
            x2 = x1;
            x1 = temp;
        }
        if (y2 < y1) {
            int temp = y2;
            y2 = y1;
            y1 = temp;
        }
        rect.setRect(x1, y1, x2 - x1, y2 - y1);

        Visualization vis = d.getVisualization();
        TupleSet focus = vis.getFocusGroup(IdeConstants.SELECTED);

        if (!e.isShiftDown()) {
            focus.clear();
        }

        // allocate the maximum space we could need

        Tuple[] selectedItems = new Tuple[vis.getGroup(GraphConstants.NODES)
                .getTupleCount()];
        Iterator it = vis.getGroup(GraphConstants.NODES).tuples();

        // in this example I'm only allowing Nodes to be selected
        int i = 0;
        while (it.hasNext()) {
            VisualItem item = (VisualItem) it.next();
            if (item.isVisible() && item.getBounds().intersects(rect)) {
                selectedItems[i++] = item;
            }
        }

        // Trim the array down to the actual size
        Tuple[] properlySizedSelectedItems = new Tuple[i];
        System
                .arraycopy(selectedItems, 0, properlySizedSelectedItems, 0,
                        i);
        for (int j = 0; j < properlySizedSelectedItems.length; j++) {
            Tuple tuple = properlySizedSelectedItems[j];
            focus.addTuple(tuple);
        }

        rubberBand.setValidated(false);
        d.repaint();
    }

    public void mouseReleased(MouseEvent e) {
        if (!SwingUtilities.isLeftMouseButton(e))
            return;
        rubberBand.setVisible(false);
        Display d = (Display) e.getComponent();

        d.setHighQuality(true);
        d.getVisualization().repaint();
    }

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

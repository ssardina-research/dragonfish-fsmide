/**
 * 
 */
package rmit.ai.bcp.fsm.ide;

import rmit.ai.bcp.fsm.FiniteStateMachine;
import rmit.ai.bcp.fsm.ide.controllers.MainController;
import rmit.ai.bcp.fsm.ide.gui.MainFrame;

/**
 * This is the main driving class of the IDE, or so called entry point
 * @author abhijeet
 */
public class DragonFish {

    /**
     * Default contructor, but is never required.
     */
    public DragonFish() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        FiniteStateMachine fsm = new FiniteStateMachine(true);
        fsm.setMachineName("Welcome");
        fsm.addNode("0", "Welcome to DragonFish - An IDE for creating Finite State Machines");
//        fsm.addNode("1", "test1");
//        fsm.addNode("2", "test2");
//        fsm.addEdge("0", "0", "edge1", true);
//        fsm.addEdge("1", "2", "edge2", true);
        MainFrame mainViewFrame = new MainFrame(fsm, fsm.getMachineName());
        MainController controller = new MainController(fsm, mainViewFrame);
        controller.toString(); // To remove "unused" warnings

        mainViewFrame.setVisible(true);
    }

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

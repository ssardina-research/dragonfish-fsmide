/**
 * 
 */
package rmit.ai.bcp.fsm.ide.utils;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * This class shows the About Box for this application and gets its version and
 * build id from its property file.
 * 
 * @author abhijeet
 */
public final class AboutBox {
    /**
     * The ResourceBundle containing some information and probably some settings
     */
    private static ResourceBundle resDragonFish   = ResourceBundle
                                                          .getBundle("DragonFish");
    /**
     * The version string for the release.
     */
    public static final String    VERSION         = resDragonFish
                                                          .getString("VERSION");
    /**
     * The Build ID of this release.
     */
    public static final String    BUILD_ID        = resDragonFish
                                                          .getString("BUILD_ID");
    /**
     * Singleton object of this class
     */
    private static AboutBox       _instance       = null;
    /**
     * Dialog Title
     */
    private static String         title           = null;
    /**
     * The About Box message
     */
    private static String         aboutGuiMessage = null;
    /**
     * Logo
     */
    private static ImageIcon      logo            = null;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * 
     */
    private AboutBox() {
        init();
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Returns the singleton instance of this class
     */
    public static AboutBox instance() {
        if (_instance == null) {
            _instance = new AboutBox();
        }
        return _instance;
    }

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */
    /**
     * Displays the About Box
     */
    public void show() {
        JOptionPane.showMessageDialog(null, aboutGuiMessage, title,
                JOptionPane.INFORMATION_MESSAGE, logo);
    }

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */
    private void init() {
        title = "About DragonFish v" + VERSION;
        aboutGuiMessage = "DragonFish v"
                + VERSION
                + "\n"
                + BUILD_ID
                + "\n\n"
                + "This is an IDE for building Finite State Machines \n"
                + "It displays state machines stored in dot format files, \n" +
                		"provides an interface to visually edit and save them. \n"
                + "DragonFish was developed by Abhijeet Anand \n" +
                		"as a part of the course \n"
                + "\"COSC1318 Internet Industry Project\"\n"
                + "\n\nVersion: " + VERSION + " by Abhijeet Anand \n"
                + "abhijeet [dot] anand [at] student [dot] rmit [dot] edu [dot] au \n";
        URL url = AboutBox.class.getResource("/images/DragonFishLogo.jpg");
        if (url != null) {
            logo = new ImageIcon(url);
        }
    }

}

/**
 * 
 */
package rmit.ai.bcp.fsm.ide.utils;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * A file extension filter to be used in JFileChoosers
 * @author Abhijeet Anand
 */
public class FileExtensionFilter extends FileFilter {

    private String ext;
    private String description;

    /*
     * =======================================================================*
     * ----------------------------- INNER CLASS -----------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ----------------------------- CONSTRUCTORS ----------------------------*
     * =======================================================================*
     */
    /**
     * Constructs a FileFilter with the given file extension
     * @param ext
     *            File extensions to accept (including the dot), e.g. ".dot"
     * @param description
     *            String describing the type of file with this extension
     */
    public FileExtensionFilter(String ext, String description) {
        this.ext = ext;
        this.description = description;
    }

    /*
     * =======================================================================*
     * ---------------------------- STATIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * ---------------------------- PUBLIC METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- ACCESSOR METHODS --------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------------- MUTATOR METHODS ---------------------------*
     * =======================================================================*
     */

    /*
     * =======================================================================*
     * --------------------- OVERRIDDEN INTERFACE METHODS --------------------*
     * =======================================================================*
     */
    /*
     * (non-Javadoc)
     * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
     */
    @Override
    public boolean accept(File f) {
        // show directories for navigation
        if (f.isDirectory()) {
            return true;
        }
        String path = f.getAbsolutePath();
        // if the file ends with the correct extension, show it
        if (ext != null) {
            if (path.endsWith(ext)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.filechooser.FileFilter#getDescription()
     */
    @Override
    public String getDescription() {
        return (description == null ? ext : description);
    }

    /*
     * =======================================================================*
     * --------------------------- UTILITY METHODS ---------------------------*
     * =======================================================================*
     */

}

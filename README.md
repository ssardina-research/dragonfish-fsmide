## Project migrated to GH: https://github.com/RMIT-STUD-PROJ/dragonfish-fsmide


JavaCC needs to be installed in the system and as ECLIPSe plugin so that the
corresponding Java sources to be obtained by compiling with JavaCC file
DOTGrammar.jjt in rmit.ai.bcp.fsm.ide.parser.dotparser. Compile that grammar
to produce corresponging java files

 sudo apt-get install javacc
 ECLIPSE plugin:  http://eclipse-javacc.sourceforge.net/
 
Then JavaCC will produce the missing .java sources from the grammar specs .jj files.

